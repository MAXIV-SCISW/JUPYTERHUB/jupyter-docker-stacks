# DEVELOPMENT

## UPDATING A KERNEL

### ADDING CONDA PACKAGE TO KERNEL

### ADDING PIP PACKAGE TO KERNEL

### ADDING GIT REPOSITORY PACKAGE TO KERNEL

### ADDING WHEEL PACKAGE TO KERNEL

#### TEST INSTALL PACKAGE FIRST
Before adding the wheel package to the image and kernel, it is probably good
to test it a bit first in a JupyterLab session to make sure everything will
work once deployed.

1. Start a JupyterLab session:

[JupyterHub](https://jupyterhub.maxiv.lu.se/)

For this example I selected the DanMax image.


2. Assuming the kernel environment you want to add to already exists, open a
   terminal and copy the yaml file used for that environment somewhere, for
   this example I am using the DanMax kernel:
```bash
git clone git@gitlab.maxiv.lu.se:scisw/jupyterhub/jupyter-docker-stacks.git
cp jupyter-docker-stacks/danmax-notebook/ubuntu-bionic/danmax-kernel-environment.yml .
```


3. Create a conda environment, giving it a new name,
   "danmax-kernel-environment-test" in this case, which will be stored in my
   local home directory:
```bash
conda env create --file danmax-kernel-environment.yml \
    --prefix ~/.conda/envs/danmax-kernel-environment-test
```


4. After the conda environment is created, see that it exists:
```bash
conda env list
```

As done in this screenshot:

![conda-env-list](screenshots/conda-env-list.png)


5. Now add your wheel package to the conda environment you just created:
```bash
~/.conda/envs/danmax-kernel-environment-test/bin/pip3 \
    install ciff-1.0.0-py3-none-any.whl
```
This command should hopefully install any necessary dependencies along with the
desired package into the environment. Some already installed packages may be
removed and then reinstalled with different versions.

If there are errors related to certain packages not being installed (because
perhaps the wheel package did not include all dependencies for some reason)
then they can be installed indivdually, and the above step can then be re-run,
for example:
```bash
~/.conda/envs/danmax-kernel-environment-test/bin/pip3 \
    install "platformdirs>=2.2.0"
~/.conda/envs/danmax-kernel-environment-test/bin/pip3 \
    install ciff-1.0.0-py3-none-any.whl
```

6. Check if the package you were trying to install exists now in the conda
   environment with:
```bash
conda list ciff --name danmax-kernel-environment-test
```
The resulting output should look something like:
```bash
    # Name     Version     Build  Channel
    ciff       0.2.0       pypi_0    pypi
```


7. Create a test kernel which makes use of your test conda environment:
```bash
envkernel conda --user --name=danmax-kernel-test \
    --display-name="DanMax Kernel Test" \
    ~/.conda/envs/danmax-kernel-environment-test
```


8. Then check that the kernel exists:
```bash
jupyter kernelspec list
```
it should show up along with other kernels that are available, such as the
preinstalled kernels:
```bash
Available kernels:
  danmax-kernel-test               /mxn/home/jasbru/.local/share/jupyter/kernels/danmax-kernel-test
  maxiv-jhub-docker-kernel-danmax  /opt/conda/share/jupyter/kernels/maxiv-jhub-docker-kernel-danmax
  python3                          /opt/conda/share/jupyter/kernels/python3
```


9. Open a new Jupyter notebook and select the test kernel (you may need to wait
   a minute until JupyterLab knows it's there).

![use-test-kernel](screenshots/use-test-kernel.png)

Then the simplest test would be to check if you can import the new module:

![import-module](screenshots/import-module.png)

If there are no errors, then it was imported successfully.

10. Ask the reasearcher for an example notebook and data if possible, then
    try it out with your test kernel.

***TODO: WRITE THIS SECTION***



#### ADD PACKAGE TO DOCKER IMAGE
If you are satisfied that the new module works (and that existing things were
not borken!) then you can add it to this repository, in which docker image
builds are kept.

1. Clone the repository if you have not yet done so (or make a fork and clone
   that), and make a new branch, which for this example involves the DanMax
   kernel:
```bash
git clone git@gitlab.maxiv.lu.se:scisw/jupyterhub/jupyter-docker-stacks.git
cd jupyter-docker-stacks/
git checkout -b adding-danamx-ciff-module master
```


2. Add the file into the directory in that contains the docker image you are
   modifying, in this example, the file is added to the DanMax image directory:
```bash
danmax-notebook/
└── ciff-1.0.0-py3-none-any.whl
```


3. Edit the Dockerfile for this image, telling it where to find the wheel file
   and how to add it to the kernel's conda environment, in this example the
   DanMax kernel.  This is done after the intitial conda environement creation,
   like in this snippet of the code:
```bash
vim danmax-notebook/ubuntu-bionic/Dockerfile
    # Create additional conda environments
    COPY danmax-notebook/ubuntu-bionic/danmax-kernel-environment.yml \
        /tmp/danmax-kernel-environment.yml
    RUN conda env create --file /tmp/danmax-kernel-environment.yml

    # Install ciff
    COPY danmax-notebook/ciff-1.0.0-py3-none-any.whl \
        /tmp/ciff-1.0.0-py3-none-any.whl
    RUN $CONDA_DIR/envs/danmax-kernel-environment/bin/pip install \
        /tmp/ciff-1.0.0-py3-none-any.whl
```

4. Then merge your branch into the master and push to the repository:
```bash
git checkout master
git fetch origin
git merge origin/master
git merge --no-ff adding-danamx-ciff-module
git push origin master
```
Pushing to the master branch will trigger the [CI pipeline](https://gitlab.maxiv.lu.se/scisw/jupyterhub/jupyter-docker-stacks/-/blob/master/.gitlab-ci.yml#L964)
which builds the image, runs some tests, pushes to the registry, and the pulls
it onto the kubernetes server.  If any of the stages fail, then subsequent
stages will not be run.

5. Watch the pipeline, as sometimes there occurs randomly some errors with
   kubernetes pods or the pulling of conda modules, and when that happens
   the stage can usually just be re-run:

[Jupyter Docker Stacks Pipelines](https://gitlab.maxiv.lu.se/scisw/jupyterhub/jupyter-docker-stacks/-/pipelines)


6. After the CI pipeline has finished, your updated kernel and image will be
   available.  Close your existing session (File > Hub Control Panel > Stop My
   Server) and start a new one.

[JupyterHub](https://jupyterhub.maxiv.lu.se/)


7. It's probably a good idea at this point to re-run the test notebook and
   check that everything is working as it should.
