###############################################################################
#
# 	Jupyter Hub Docker Stacks - Makefile
#
#    This Makefile is used for building the jupyterlab docker
#    images locally using docker.
#    Images that are to be used for deployment are created
#    in the CI pipeline. See .gitlab-ci.yml
#
#
#  	Run 'make' for usage information
#
###############################################################################

# Makefile stuff
.DEFAULT_GOAL := help
SHELL := /bin/bash
.ONESHELL:
THIS_FILE := $(lastword $(MAKEFILE_LIST))

# Colors
LIGHTPURPLE := \033[1;35m
GREEN := \033[32m
CYAN := \033[36m
BLUE := \033[34m
RED := \033[31m
NC := \033[0m

# Separator between output, 80 characters wide
define print_separator
	printf "$1"; printf "%0.s*" {1..80}; printf "$(NC)\n"
endef
print_line_green = $(call print_separator,$(GREEN))
print_line_blue = $(call print_separator,$(BLUE))
print_line_red = $(call print_separator,$(RED))


###############################################################################
##@ Help
###############################################################################

.PHONY: help

help:  ## Display this help message
	@printf "\n"
	$(print_line_blue)
	printf "$(BLUE)Jupyter Docker Stacks $(CYAN)Makefile$(NC)\n"
	printf "    This Makefile is used for building the jupyterlab docker\n"
	printf "    images locally using docker.\n"
	printf "    Images that are to be used for deployment are created \n"
	printf "    in the CI pipeline. See .gitlab-ci.yml\n"
	$(print_line_blue)
	printf "\n"
	$(print_line_blue)
	printf "$(BLUE)Usage\n    $(CYAN)make $(NC)<target>\n"
	printf "\n$(BLUE)Examples$(NC)\n"
	printf "    $(CYAN)make $(NC)build-hdf5 \n"
	awk 'BEGIN {FS = ":.*##";} /^[a-zA-Z_-].*##/ \
	{ printf "    $(CYAN)%-23s$(NC) %s\n", $$1, $$2} /^##@/ \
	{ printf "\n$(BLUE)%s$(NC)\n", substr($$0, 5) } ' $(MAKEFILE_LIST)
	$(print_line_blue)


###############################################################################
##@ Build Images - For local use, with docker
###############################################################################

.PHONY: build-juptyer-base

build-juptyer-base: ## Build the Jupyter Base image with docker
	@printf "\n"
	$(print_line_blue)

	docker build --build-arg NOTEBOOK_VERSION=jupyter-base-test:testing \
		-t jupyter-base-test:testing \
		-f jupyter-base/ubuntu-bionic-cuda-12-2/Dockerfile .

	$(print_line_blue)
	printf "\n"

build-base-new: ## Build the Base New image with docker
	@printf "\n"
	$(print_line_blue)

	docker build --build-arg NOTEBOOK_VERSION=base-new-notebook:testing \
		--build-arg IMG_FROM=jupyter-base-test:testing \
		-t base-new-notebook:testing \
		-f base-new-notebook/ubuntu-bionic-22-4/Dockerfile .

	$(print_line_blue)
	printf "\n"

build-hdf5: ## Build the HDF5 image with docker
	@printf "\n"
	$(print_line_blue)

	docker build --build-arg NOTEBOOK_VERSION=hdf5-notebook:testing \
		--build-arg IMG_FROM=base-new-notebook:testing \
		-t hdf5-notebook:testing \
		-f hdf5-notebook/ubuntu-bionic-22-4/Dockerfile .

	$(print_line_blue)
	printf "\n"

run-juptyer-base: ## Run the Jupyter Base image in bash shell
	docker run -it --entrypoint bash jupyter-base-test:testing

run-base-new: ## Run the Base New image in bash shell
	docker run -it --entrypoint bash base-new-notebook:testing

run-hdf5: ## Run the HDF5 image in bash shell
	docker run -it --entrypoint bash hdf5-notebook:testing

run-dev-image: ## Run the HDF5 image in bash shell
	docker run \
		-it --entrypoint bash \
		--device /dev/fuse \
		-v $(PWD)/singularity-defs:/singularity-defs \
		-v $(PWD)/singularity-sifs:/singularity-sifs \
		harbor.maxiv.lu.se/jupyterhub/development-notebook-ubuntu-bionic:stable

rm-juptyer-base: ## Remove the Jupyter Base image
	docker image rm jupyter-base-test:testing -f

rm-base-new: ## Remove the Base New image
	docker image rm base-new-notebook:testing -f

rm-hdf5: ## Remove the HDF5 image
	docker image rm hdf5-notebook:testing -f
