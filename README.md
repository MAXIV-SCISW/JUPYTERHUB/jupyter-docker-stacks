# DOCKER STACKS

## OVERVIEW

This is a collection of jupyter notebook images used at MAX IV.

Other examples of notebook images can be found here:

[University of Copenhagen - jupyter notebooks](https://github.com/rasmunk/nbi-jupyter-docker-stacks)

[Official Jupyter Docker Stacks](https://github.com/jupyter/docker-stacks)

The notebook images in this repository are modified versions of the offical
docker stack images.

## IMAGE REQUIREMENTS
The images used by the jupyterhub installation at MAXIV have a few
requirements:
* The base for the images comes from the one of the
[Official Jupyter Docker Stacks](https://github.com/jupyter/docker-stacks)
* The user is switched to root before the end of the Dockerfile - though the
  actual jupyter notebook (kernel) will be run in the end as the logged-in
  user, via sudo
* A customized start.sh script is used, as in the base-notebook/ in this
  repository

## GIT CODE
```bash
git clone git@gitlab.maxiv.lu.se:scisw/jupyterhub/jupyter-docker-stacks.git
```

## MAKEFILE
There is a makefile to handle most tasks:
```bash
cd jupyter-docker-stacks/
make
```
![screenshot](screenshots/makefile_screenshot.png)

## BUILD IMAGES
The images are setup to be built one upon the other, starting with
basic Jupyter dependencies, and moving up to additioanl CUDA libraries and
more specific libraries required by different notebooks and kernels.

The "base" images need to be built in this order:
```bash
make build/jupyter-base-ubuntu-bionic
make build/cuda-notebook-ubuntu-bionic
make build/base-notebook-ubuntu-bionic
```

After the above three images are built, the others can then also be built
in any order, and each one depends on the base-notebook-ubuntu-bionic
existing:
```bash
make build/bloch-notebook-ubuntu
make build/conda-notebook-ubuntu
make build/hdf5-notebook-ubuntu
make build/matlab-notebook-ubuntu
make build/tomography-notebook-ubuntu
```

## USE IMAGES
The images need to be added to the list of options in the jupyterhub config
file.

Example:
```bash
cd /var/www/jupyter_service/
vim maxiv/jupyterhub_config.py
	# Available docker images the user can spawn
	c.SwarmSpawner.dockerimages = [
		{'name': 'HDF5',
		 'image': 'maxiv-scisw-jupyter/hdf5-notebook-ubuntu:stable'},
		{'name': 'Matlab',
		 'image': 'maxiv-scisw-jupyter/matlab-notebook-ubuntu:stable'},
	]
```

The jupyterhub service then needs to be restarted after editing the
jupyterhub_config.py file.


## IMAGE REPOSITORY
When the CI is triggered on the master branch (only at MAX IV currently),
docker images are pushed to both the internal MAX IV image registry, and to a
publicly available registry.

### MAX IV
Registry name: harbor.maxiv.lu.se

Web interface: [harbor.maxiv.lu.se > jupyterhub](https://harbor.maxiv.lu.se/harbor/projects/41/repositories)

### PUBLIC
Registry name: registry.gitlab.com

Web interface: [gitlab.com](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jupyter-docker-stacks/container_registry/)

For some information about using the images, see the
[wiki page](https://gitlab.com/MAXIV-SCISW/JUPYTERHUB/jupyter-docker-stacks/-/wikis/home)

## IMAGE DEPLOYMENT
The CI is setup so that when changes are pushed to the master branch, new
images will be pulled from the registry onto the K8S cluster and be made
available in all installations:
  * **prod** https://jupyterhub.maxiv.lu.se
  * **review**: https://jupyterkub-review.maxiv.lu.se
  * **next**: https://jupyterkub-next.maxiv.lu.se


## DEVELOPMENT
To change an existing image or kernel, or to make new ones, see:

[CONTRIBUTING.md](CONTRIBUTING.md)
