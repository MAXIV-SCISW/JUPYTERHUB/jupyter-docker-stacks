#!/bin/bash
# Copyright (c) Jupyter Development Team.
# Distributed under the terms of the Modified BSD License.

set -e

# Exec the specified command or fall back on bash
if [ $# -eq 0 ]; then
    cmd=bash
else
    cmd=$*
fi

run-hooks () {
    # Source scripts or run executable files in a directory
    if [[ ! -d "$1" ]] ; then
	return
    fi
    echo "$0: running hooks in $1"
    for f in "$1"/*; do
	case "$f" in
	    *.sh)
		echo "$0: running $f"
		source "$f"
		;;
	    *)
		if [[ -x "$f" ]] ; then
		    echo "$0: running $f"
		    "$f"
		else
		    echo "$0: ignoring $f"
		fi
		;;
	esac
    echo "$0: done running hooks in $1"
    done
}

# Modify the html pages a bit - dumb place to have this?
modify_html () {
    # Change title
    sed -i.bak 's/JupyterLab/JupyterLab - MAX IV/' /opt/conda/share/jupyter/lab/static/index.html
}

run-hooks /usr/local/bin/start-notebook.d

# Handle special flags if we're root
if [ $(id -u) == 0 ] ; then

    printenv

    # Convert specific strings to arrays
    NB_GROUPS=($NB_GROUPS)
    NB_GIDS=($NB_GIDS)
    ALL_GROUPS=($ALL_GROUPS)
    ALL_GIDS=($ALL_GIDS)
    ALL_USERS=($ALL_USERS)
    ALL_UIDS=($ALL_UIDS)

    # Modify the html pages a bit - dumb place to have this?
    modify_html || true

    # Only attempt to change the jovyan username if it exists
    if id jovyan &> /dev/null ; then

		# First set the new home directory for $NB_USER to a temporary
		# directory, inorder to avoid the chown on the real home directory
		# that "usermod -u" performs
        echo " *  setting username to: $NB_USER"

        usermod -d /tmp/home/$NB_USER -l $NB_USER jovyan

		# Change UID of NB_USER to NB_UID if it does not match
		if [ "$NB_UID" != $(id -u $NB_USER) ] ; then
			echo " *  setting $NB_USER uid to: $NB_UID"
			usermod -u $NB_UID $NB_USER -o
		fi

        # Create more groups inside the container - not really necessary but
        # nice to have when examining file ownership & permissions
        for index in ${!ALL_GROUPS[*]}; do
            if grep -q ${ALL_GROUPS[$index]} /etc/group ; then
                echo "--x group ${ALL_GROUPS[$index]} (${ALL_GIDS[$index]}) already exists"
            else
                echo "--> creating group: ${ALL_GROUPS[$index]} (${ALL_GIDS[$index]})"
                groupadd -g ${ALL_GIDS[$index]} -o ${ALL_GROUPS[$index]}
            fi
        done

        for index in ${!NB_GROUPS[*]}; do
            if grep -q ${NB_GROUPS[$index]} /etc/group ; then
                echo "--x group ${NB_GROUPS[$index]} (${NB_GIDS[$index]}) already exists"
            else
                echo "--> creating group: ${NB_GROUPS[$index]} (${NB_GIDS[$index]})"
                groupadd -g ${NB_GIDS[$index]} -o ${NB_GROUPS[$index]}
            fi
        done

        # Add some users - just beamline service accounts - not necessary but
        # nice to have perhaps
        for index in ${!ALL_USERS[*]}; do
            if grep -q ${ALL_USERS[$index]} /etc/passwd ; then
                echo "--x user ${ALL_USERS[$index]} (${ALL_UIDS[$index]}) already exists"
            else
                echo "--> creating user: ${ALL_USERS[$index]} (${ALL_UIDS[$index]})"
                useradd -M -u ${ALL_UIDS[$index]} ${ALL_USERS[$index]}
            fi
        done

        # Set the primary gid for NB_USER to NB_GID.
		if [ "$NB_GID" != $(id -g $NB_USER) ] ; then
			echo " *  setting primary group of $NB_USER to $NB_GID"
			usermod -g $NB_GID $NB_USER
		fi

        # Add the user to additional groups - again, not necessary, but nice to
        # have
        for index in ${!NB_GROUPS[*]}; do
			echo "--> add $NB_USER to group: ${NB_GROUPS[$index]} (${NB_GIDS[$index]})"
			usermod -a -G ${NB_GIDS[$index]} $NB_USER
        done

        # Add to the 'users' group (100) in jupyterhub in order to allow
        # installation of conda packages
        usermod -a -G 100 $NB_USER

		# Now set the user's home directory properly. This does not execute
		# chown.
		usermod -d $HOME_DIR $NB_USER
    fi

	echo "*** Done with user and group creation ***"
    echo "Will run jupyterlab as user: $NB_USER - $(id $NB_USER)"

    # # Handle case where provisioned storage does not have the correct
    # # permissions by default # Ex: default NFS/EFS (no auto-uid/gid)
    # if [[ "$CHOWN_HOME" == "1" || "$CHOWN_HOME" == 'yes' ]]; then
    #     echo "Changing ownership of $HOME_DIR to $NB_UID:$NB_GID"
    #     chown $CHOWN_HOME_OPTS $NB_UID:$NB_GID $HOME_DIR
    # fi
    # if [ ! -z "$CHOWN_EXTRA" ]; then
    #     for extra_dir in $(echo $CHOWN_EXTRA | tr ',' ' '); do
    #         chown $CHOWN_EXTRA_OPTS $NB_UID:$NB_GID $extra_dir
    #     done
    # fi

    # handle home and working directory if the username changed
    if [[ "$NB_USER" != "jovyan" ]]; then

        # If the notebook (work) directory does not exist, create it as $NB_USER
        echo "Creating work directory: $NOTEBOOK_DIR"
		sudo -E -H -u $NB_USER sh -c "mkdir -p $NOTEBOOK_DIR"

        # Direct JupyterLab output into a log file that the user can access:
        user_log_file=$NOTEBOOK_DIR/jupyterlab_$(date +"%Y-%m-%d_%H-%M-%S").log
        cmd="$cmd 2>&1 | tee $user_log_file"

        # Clean up old log files
		echo "Clean up old log files"
        sudo -E -H -u $NB_USER sh -c \
            "find $NOTEBOOK_DIR -maxdepth 1 -name jupyterlab_*.log -mtime +7 -delete"

		# Add some symbolic links to data and shared directories inside the
		# work directory
		echo "Creating symbolic links inside $NOTEBOOK_DIR"
		sudo -E -H -u $NB_USER sh -c "chmod 770 $NOTEBOOK_DIR"
		sudo -E -H -u $NB_USER sh -c "mkdir -p $NOTEBOOK_DIR/mxn"
		sudo -E -H -u $NB_USER sh -c "mkdir -p $NOTEBOOK_DIR/mxn/home"
		sudo -E -H -u $NB_USER sh -c "ln -sf $HOME_DIR $NOTEBOOK_DIR/mxn/home/."
		sudo -E -H -u $NB_USER sh -c "ln -sf /mxn/groups $NOTEBOOK_DIR/mxn/."
		sudo -E -H -u $NB_USER sh -c "ln -sf /data $NOTEBOOK_DIR/."

		# Fix things for installing conda packages on debian/ubuntu
		echo "Setting up local user conda directories"
		sudo -E -H -u $NB_USER sh -c "mkdir -p $HOME_DIR/.conda/pkgs/"
		sudo -E -H -u $NB_USER sh -c "mkdir -p $HOME_DIR/.conda/envs/"
		sudo -E -H -u $NB_USER sh -c "touch $HOME_DIR/.conda/envs/.conda_envs_dir_test"
		sudo -E -H -u $NB_USER sh -c "touch $HOME_DIR/.conda/pkgs/urls"
		sudo -E -H -u $NB_USER sh -c "chmod 774 $HOME_DIR/.conda/pkgs/urls"
		sudo -E -H -u $NB_USER sh -c "touch $HOME_DIR/.conda/environments.txt"

    fi

    # Enable sudo if requested
    if [[ "$GRANT_SUDO" == "1" || "$GRANT_SUDO" == 'yes' ]]; then
        echo "--> granting $NB_USER sudo access"
        echo "$NB_USER ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/notebook
    fi

    # Add $CONDA_DIR/bin to sudo secure_path
    echo "--> appending $CONDA_DIR/bin to sudo PATH"
    sed -r "s#Defaults\s+secure_path=\"([^\"]+)\"#Defaults secure_path=\"\1:$CONDA_DIR/bin\"#" /etc/sudoers | grep secure_path > /etc/sudoers.d/path

    # Exec the command as NB_USER with the PATH and the rest of the environment
    # preserved. Change to the $NB_USER home directory as that user, then
    # execute the command
    run-hooks /usr/local/bin/before-notebook.d
    echo "Executing the command: $cmd"
    exec sudo -E -H -u $NB_USER PATH=$PATH XDG_CACHE_HOME=$HOME_DIR/.cache PYTHONPATH=$PYTHONPATH sh -c "cd $HOME_DIR; $cmd"


# Run other things if not root
else
    if [[ "$NB_UID" == "$(id -u jovyan)" && "$NB_GID" == "$(id -g jovyan)" ]]; then
        # User is not attempting to override user/group via environment
        # variables, but they could still have overridden the uid/gid that
        # container runs as. Check that the user has an entry in the passwd
        # file and if not add an entry.
        whoami &> /dev/null || STATUS=$? && true
        if [[ "$STATUS" != "0" ]]; then
            if [[ -w /etc/passwd ]]; then
                echo "Adding passwd file entry for $(id -u)"
                cat /etc/passwd | sed -e "s/^jovyan:/nayvoj:/" > /tmp/passwd
                echo "jovyan:x:$(id -u):$(id -g):,,,:/home/jovyan:/bin/bash" >> /tmp/passwd
                cat /tmp/passwd > /etc/passwd
                rm /tmp/passwd
            else
                echo 'Container must be run with group "root" to update passwd file'
            fi
        fi

        # Warn if the user isn't going to be able to write files to $HOME.
        if [[ ! -w /home/jovyan ]]; then
            echo 'Container must be run with group "users" to update files'
        fi
    else
        # Warn if looks like user want to override uid/gid but hasn't
        # run the container as root.
        if [[ ! -z "$NB_UID" && "$NB_UID" != "$(id -u)" ]]; then
            echo 'Container must be run as root to set $NB_UID'
        fi
        if [[ ! -z "$NB_GID" && "$NB_GID" != "$(id -g)" ]]; then
            echo 'Container must be run as root to set $NB_GID'
        fi
    fi

    # Warn if looks like user want to run in sudo mode but hasn't run
    # the container as root.
    if [[ "$GRANT_SUDO" == "1" || "$GRANT_SUDO" == 'yes' ]]; then
        echo 'Container must be run as root to grant sudo permissions'
    fi

    # Execute the command
    run-hooks /usr/local/bin/before-notebook.d
    echo "Executing the command: $cmd"
    exec $cmd
fi
