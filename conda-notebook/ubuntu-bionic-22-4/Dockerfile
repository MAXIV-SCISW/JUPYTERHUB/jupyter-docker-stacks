# Start from the MAX IV base-notebook-ubuntu-bionic image
ARG IMG_FROM=unspecified 
FROM $IMG_FROM 

# Set environmental variables
ARG NOTEBOOK_VERSION=unspecified
ENV NOTEBOOK_VERSION="${NOTEBOOK_VERSION}"

# Switch to jovyan user for installing conda packages
ARG NB_USER="jovyan"
ARG NB_UID="1000"
ARG NB_GID="100"
USER $NB_UID

# Install python libraries from conda yaml into base environment 
COPY conda-notebook/ubuntu-bionic-22-4/conda-base-environment.yml \
    /tmp/conda-base-environment.yml
RUN mamba env update --name base --file /tmp/conda-base-environment.yml

# Replace jupyter_config.json with setting to change format of conda kernel
# display names set by nb_conda_kernels
# Available configuration options:
#   https://github.com/Anaconda-Platform/nb_conda_kernels/blob/master/README.md
USER root
COPY conda-notebook/ubuntu-bionic-22-4/jupyter_config.json \
    $CONDA_DIR/etc/jupyter/jupyter_config.json
RUN chown $NB_UID:$NB_GID $CONDA_DIR/etc/jupyter/jupyter_config.json
USER $NB_UID

# Export base and kernel environment for later use
RUN $CONDA_DIR/bin/conda-env export --no-builds --name base \
    --file $CONDA_DIR/conda-meta/conda-base-environment-exported.yml

# Clean-up and fix the permissions on conda files
RUN mamba clean --all -f -y \
 && find /opt/conda/ -follow -type f -name '*.a' -delete \
 && find /opt/conda/ -follow -type f -name '*.pyc' -delete \
 && find /opt/conda/ -follow -type f -name '*.js.map' -delete
RUN fix-permissions $CONDA_DIR 

# Switch to root user
USER root

# # Update locate database
# RUN updatedb
# RUN chmod 664 /var/lib/plocate/plocate.db
