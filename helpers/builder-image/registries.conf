# For more information on this configuration file, see containers-registries.conf(5).
#
# NOTE: RISK OF USING UNQUALIFIED IMAGE NAMES
# We recommend always using fully qualified image names including the registry
# server (full dns name), namespace, image name, and tag
# (e.g., registry.redhat.io/ubi8/ubi:latest). Pulling by digest (i.e.,
# quay.io/repository/name@digest) further eliminates the ambiguity of tags.
# When using short names, there is always an inherent risk that the image being
# pulled could be spoofed. For example, a user wants to pull an image named
# `foobar` from a registry and expects it to come from myregistry.com. If
# myregistry.com is not first in the search list, an attacker could place a
# different `foobar` image at a registry earlier in the search list. The user
# would accidentally pull and run the attacker's image and code rather than the
# intended content. We recommend only adding registries which are completely
# trusted (i.e., registries which don't allow unknown or anonymous users to
# create accounts with arbitrary names). This will prevent an image from being
# spoofed, squatted or otherwise made insecure.  If it is necessary to use one
# of these registries, it should be added at the end of the list.
#
# # An array of host[:port] registries to try when pulling an unqualified image, in order.
unqualified-search-registries = ["docker.io"]

# Cache for Dockerhub
[[registry]]
prefix = "docker.io"
insecure = false
blocked = false
location = "docker.io"

# main production cache on OKD
[[registry.mirror]]
location = "harbor.maxiv.lu.se/dockerhub"

# replicated production cache on OKDev
[[registry.mirror]]
location = "harbor-ro.apps.okdev.maxiv.lu.se/dockerhub"

# standalone cache on OKDev
[[registry.mirror]]
location = "harbor-ro.apps.okdev.maxiv.lu.se/dockerhub-cache"

# Cache for Quay.io
[[registry]]
prefix = "quay.io"
insecure = false
blocked = false
location = "quay.io"

[[registry.mirror]]
location = "harbor.maxiv.lu.se/quay.io"

[[registry.mirror]]
location = "harbor-ro.apps.okdev.maxiv.lu.se/quay.io"